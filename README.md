# README

This is a project to practice my ReactJS skills.
It is a basic web dashboard with various little apps.

## App Information

App Name: dashboard

Created: November/December 2016

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/dashboard)

## Notes

* Various tutorials and instructions were followed.
* Used 'create-react-app' [Link](https://github.com/facebookincubator/create-react-app)
* Used 'react-clock' [Link](https://www.npmjs.com/package/react-clock)

Last updated: 2024-11-15
