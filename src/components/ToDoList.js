import React from 'react';
import ToDoItems from './ToDoItems';

class ToDoList extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {items: [], text: ''};
  }

  render() {
    return (
      <div className="to-do-list">
        <form onSubmit={this.handleSubmit} className="form-group">
          <div className="input-group">
            <input
              onChange={this.handleChange}
              value={this.state.text}
              type="text"
              className="form-control" />
            <span className="input-group-btn">
              <button className="btn btn-md btn-custom">Add</button>
            </span>
          </div>
        </form>

        <ToDoItems items={this.state.items} />

      </div>
    );
  }

  handleChange(e) {
    this.setState({
      text: e.target.value
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    var newItem = {
      text: this.state.text,
      id: Date.now()
    };
    this.setState((prevState) => ({
      items: prevState.items.concat(newItem),
      text: ''
    }));
  }
}

export default ToDoList;