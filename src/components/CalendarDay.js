import React from 'react';

var CalendarDay = React.createClass({
  render: function () {
    var dayNames = [
      "Sunday", "Monday", "Tuesday", "Wednesday",
      "Thursday", "Friday", "Saturday"
    ];

    var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ];

    var date = new Date();
    var dayIndex = date.getDay();
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return (
      <div>
        <h3>{dayNames[dayIndex] + ', '}</h3>
        <h3>{day + ' ' + monthNames[monthIndex] + ' ' + year}</h3>
      </div>
    );
  }
});

export default CalendarDay;