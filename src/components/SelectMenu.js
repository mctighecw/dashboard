import React from 'react';

var SelectMenu = React.createClass({
  getInitialState: function () {
    return {
      value: "choose"
    };
  },

  handleChange: function (e) {
    var weather = e.target.value;
    this.setState({
      value: e.target.value
    });
    this.props.onChange(weather);
  },
  
  render: function () {
    return (
      <div>
        <select 
          value={this.state.value}
          onChange={this.handleChange}>
          
          <option value="choose" disabled>Choose here</option>
          <option value="sunny">Sunny</option>
          <option value="cloudy">Cloudy</option>
          <option value="rainy">Rainy</option>
          <option value="snowy">Snowy</option>
          <option value="stormy">Stormy</option>
        </select>
      </div>
    );
  }
});

export default SelectMenu;