import React from 'react';

var Nav = React.createClass({
  render: function () {
    var reactPic = {
      src: require("../resources/images/react.png"),
      alt: "React Dashboard",
      title: "Built with ReactJS"
    };

    return (
      <nav>
        <img src={reactPic.src} alt={reactPic.alt} title={reactPic.title} id="react-pic" />
        <h1>Dashboard</h1>
      </nav>
    );
  }
});

export default Nav;