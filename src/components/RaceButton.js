import React from 'react';

var RaceButton = React.createClass({
  render: function () {
    return (
      <div>
        <button
          onClick={this.props.onClick}
          className="btn btn-md btn-custom">
            {this.props.raced ? 'Race again' : 'Race now'}
        </button>
      </div>
    );
  }
});

export default RaceButton;