import React from 'react';
import DogPic from './DogPic';
import DogMenu from './DogMenu';

var PICTURES = {
  beagle: require('../resources/images/dogs/beagle.jpg'),
  dachshund: require('../resources/images/dogs/dachshund.jpg'),
  goldenRetriever: require('../resources/images/dogs/golden_retriever.jpg'),
  jackRussell: require('../resources/images/dogs/jack_russell.jpg'),
  shiba: require('../resources/images/dogs/shiba.jpg')
};

var FavoriteDogs = React.createClass({
  getInitialState: function () {
    return { src: '' };
  },
  
  choosePic: function (newPic) {
    this.setState({
      src: PICTURES[newPic]
    });
  },

  render: function () {
    return (
      <div>
        <DogMenu choosePic={this.choosePic} />
        <DogPic src={this.state.src} />
      </div>
    );
  }
});

export default FavoriteDogs;