import React from 'react';
import SelectMenu from './SelectMenu';
import DisplayMenu from './DisplayMenu';

var WeatherMenu = React.createClass({
  getInitialState: function () {
    return { weather: '' };
  },
  
  changeWeather: function (newWeather) {
    this.setState({
      weather: newWeather
    });
  },

  render: function () {
    return (
      <div>
        <SelectMenu onChange={this.changeWeather} />
        <br />
        <DisplayMenu weather={this.state.weather} />
      </div>
    );
  }
});

export default WeatherMenu;