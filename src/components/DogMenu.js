import React from 'react';

var DogMenu = React.createClass({
  handleClick: function (e) {
    var image = e.target.value;
    this.props.choosePic(image);
  },
  
  render: function () {
    return (
      <form onClick={this.handleClick}>
        <input type="radio" name="img" value="beagle" />Beagle
        <input type="radio" name="img" value="dachshund" />Dachshund
        <input type="radio" name="img" value="goldenRetriever" />Golden Retriever
        <br />
        <input type="radio" name="img" value="jackRussell" />Jack Russell
        <input type="radio" name="img" value="shiba" />Shiba
      </form>
    );
  }
});

export default DogMenu;