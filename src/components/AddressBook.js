import React from 'react';

var AddressBook = React.createClass({
  getInitialState: function () {
    return {
      password: 'myfriends',
      authorized: false
    };
  },

  authorize: function (e) {
    var password = e.target.querySelector('input[type="password"]').value;
    var auth = password === this.state.password;
    this.setState({
      authorized: auth
    });
  },

  render: function () {
    var login = (
      <div className="addressbook-authenticate">
        <form action="#" onSubmit={this.authorize} className="form-group">
          <div className="input-group">
            <input
              type="password"
              placeholder="Password"
              className="form-control" />
            <span className="input-group-btn">
              <button className="btn btn-md btn-custom">Submit</button>
            </span>
          </div>
        </form>
        <p>Hint: myfriends</p>
      </div>
    );

    var addresses = (
      <div className="addressbook-list">
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Phone</th>
              <th>E-Mail</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>John Smith</td>
              <td>555-453-2981</td>
              <td>john@smith.com</td>
            </tr>
            <tr>
              <td>Susan Jones</td>
              <td>555-725-5623</td>
              <td>susan@jones.com</td>
            </tr>
            <tr>
              <td>Sam Adams</td>
              <td>555-814-4827</td>
              <td>sam@adams.com</td>
            </tr>
            <tr>
              <td>Kate Clark</td>
              <td>555-627-9833</td>
              <td>kate@clark.com</td>
            </tr>
            <tr>
              <td>Sarah Williams</td>
              <td>555-925-1388</td>
              <td>sarah@williams.com</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
    
    return (
      <div id="authorization">
        <h4>
        { this.state.authorized ? 'Contact details:' : 'Please authenticate' }
        </h4>
        { this.state.authorized ? addresses : login }
      </div>
    );
  }
});

export default AddressBook;