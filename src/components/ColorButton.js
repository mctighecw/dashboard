import React from 'react';

var ColorButton = React.createClass({
  render: function () {
    return (
      <button
        className="btn btn-lg btn-custom"
        id="color-button"
        onClick={this.props.onClick}>
          New color
      </button>
    );
  }
});

export default ColorButton;