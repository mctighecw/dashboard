import React from 'react';

var Input = React.createClass({
  getInitialState: function () {
    return {
      userInput: ''
    };
  },
  
  handleUserInput: function (e) {
    this.setState({
      userInput: e.target.value
    });
  },
  
  render: function () {
    return (
      <div>
        <input
          type="text"
          onChange={this.handleUserInput}
          value={this.state.userInput}
          className="form-control" />

        <br />

        <div className="user-output">
          <h4 className="user-output-text">{this.state.userInput}</h4>
        </div>
      </div>
    );
  }
});

export default Input;