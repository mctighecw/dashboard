import React from 'react';

var DogPic = React.createClass({
  render: function () {
    return (
      <div>
        <img src={this.props.src} className="img-responsive dog-picture" role="presentation" />
      </div>
    );
  }
});

export default DogPic;