import React from 'react';

var AdviceButton = React.createClass({
  getAdvice: function () {
    var randomNumber = Math.floor(Math.random() * 9);
    var todaysAdvice;

    switch (randomNumber) {
    case 0:
      todaysAdvice = "Don't count your chickens before they hatch.";
      break;
    case 1:
      todaysAdvice =  "Nothing ventured, nothing gained.";
      break;
    case 2:
      todaysAdvice =  "The grass always looks greener on the other side.";
      break;
    case 3:
      todaysAdvice =  "If you don't succeed at first, try, try again.";
      break;
    case 4:
      todaysAdvice =  "Every cloud has a silver lining.";
      break;
    case 5:
      todaysAdvice =  "The early bird gets the worm.";
      break;
    case 6:
      todaysAdvice =  "What goes around comes around.";
      break;
    case 7:
      todaysAdvice =  "Pride always goes before a fall.";
      break;
    case 8:
      todaysAdvice =  "Tomorrow will be a new day.";
      break;
    default:
      todaysAdvice = 'Something went wrong.';
      break;
    }

    alert(todaysAdvice);
  },

  render: function () {
    return (
      <div>
        <h4>Feeling confused? Want some help?</h4>
        <br />
        <button onClick={this.getAdvice} className="btn btn-lg btn-custom" id="advice-button">Free advice</button>
      </div>
    );
  }
});

export default AdviceButton;