import React from 'react';

var DisplayMenu = React.createClass({
  render: function () {
    var weather = this.props.weather;
    var advice;
    var icon;
    var weatherIcon = {
      sunny: require('../resources/icons/weather/sun.png'),
      cloudy: require('../resources/icons/weather/cloud.png'),
      rainy: require('../resources/icons/weather/rain.png'),
      snowy: require('../resources/icons/weather/snow.png'),
      stormy: require('../resources/icons/weather/storm.png'),
      suncloud: require('../resources/icons/weather/suncloud.png')
    };

    if (weather === 'sunny') {
      advice = <h3>Sunny, great! You better go outside and enjoy the day!</h3>;
      icon = <img src={weatherIcon.sunny} className="weather-icon" role="presentation" />;
    } else if (weather === 'cloudy') {
      advice = <h3>Cloudy, oh no! Not every day can be nice weather.</h3>;
      icon = <img src={weatherIcon.cloudy} className="weather-icon" role="presentation" />;
    } else if (weather === 'rainy') {
      advice = <h3>Rainy, better stay inside! It is a good day to work.</h3>;
      icon = <img src={weatherIcon.rainy} className="weather-icon" role="presentation" />;
    } else if (weather === 'snowy') {
      advice = <h3>Snowy, winter must be here! It is time to go skiing!</h3>;
      icon = <img src={weatherIcon.snowy} className="weather-icon" role="presentation" />;
    } else if (weather === 'stormy') {
      advice = <h3>Stormy, hopefully the power will stay on!</h3>;
      icon = <img src={weatherIcon.stormy} className="weather-icon" role="presentation" />;
    } else {
      advice = <h3></h3>;
      icon = <img src={weatherIcon.suncloud} className="weather-icon" role="presentation" />;
    }

    return (
      <div>
        {icon}
        {advice}
      </div>
    );
  }
});

export default DisplayMenu;