import React from 'react';

var ToDoItems = React.createClass({
  render() {
    var itemsList = (
      <div className="items-list">
        <h3>To Do:</h3>
        <div>
          {this.props.items.map(item => (
            <p key={item.id}>{item.text}</p>
          ))}
        </div>
      </div>
    );

    var noItems = (
      <p><i>Nothing on the list</i></p>
    );

    return (
      <div>
        { this.props.items !== 0 ? itemsList : noItems }
      </div>
    );
  }
});

export default ToDoItems;