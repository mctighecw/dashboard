import React from 'react';
import ColorButton from './ColorButton';

var RandomColor = React.createClass({
  getInitialState: function () {
    return { color: [255, 255, 255] }
  },
  
  componentDidMount: function () {
    this.applyColor();
  },

  componentDidUpdate: function (prevProps, prevState) {
    this.applyColor();
  },

  formatColor: function (ary) {
    return 'rgb(' + ary.join(', ') + ')';
  },

  applyColor: function () {
    var color = this.formatColor(this.state.color);
    document.getElementById("color-component").style.background = color;
  },

  chooseColor: function () {
    for (var i = 0, random = []; i < 3; i++) {
      random.push(Math.floor(Math.random() * 256));
    }
    return random; 
  },
  
  handleClick: function () {
    this.setState({
      color: this.chooseColor()
    });
  },

  render: function () {
    return (
      <div>
        <ColorButton
          onClick={this.handleClick} />
        <h4 id="your-color">
          Your color is {this.formatColor(this.state.color)}.
        </h4>
      </div>
    );
  }
});

export default RandomColor;