import React from 'react';
import RaceButton from './RaceButton';

var Racing = React.createClass({
  getInitialState: function () {
    return {
      raceText: 'Get ready for a car race.',
      racePic:  require('../resources/icons/racing/race_start.png'),
      raced: false
    };
  },

  raceReady: function () {
    this.setState({
      raceText: 'Get ready, set...',
      racePic: require('../resources/icons/racing/driver.png')
    });
  },

  raceStart: function () {
    this.setState({
      raceText: 'GO!',
      racePic: require('../resources/icons/racing/green_flag.png')
    });
  },

  raceCar: function () {
    this.setState({
      raceText: 'Racing...',
      racePic: require('../resources/icons/racing/race_car.png')
    });
  },

  decideWinner: function () {
    var randomNumber = Math.floor(Math.random() * 2);

    if (randomNumber === 1) {
      this.setState({
        raceText: 'Congratulations, you won!',
        racePic: require('../resources/icons/racing/trophy.png'),
        raced: true
      });
    } else {
      this.setState({
        raceText: 'Sorry, you lost.',
        racePic: require('../resources/icons/racing/checkered_flag.png'),
        raced: true
      });
    }
  },

  handleClick: function () {
    setTimeout(this.raceReady, 1000);
    setTimeout(this.raceStart, 2000);
    setTimeout(this.raceCar, 3000);
    setTimeout(this.decideWinner, 6000);
  },

  render: function () {
    return (
      <div>
        <div>
          <h4>{this.state.raceText}</h4>
          <img src={this.state.racePic} className="img-responsive race-picture" role="presentation" />
        </div>
        <RaceButton onClick={this.handleClick} raced={this.state.raced} />
      </div>
    );
  }
});

export default Racing;