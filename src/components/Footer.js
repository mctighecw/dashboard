import React from 'react';

var Footer = React.createClass({
  render: function () {
    return (
      <footer>
        <h4>&copy; 2016, Christian McTighe. Coded by Hand.</h4>
      </footer>
    );
  }
});

export default Footer;
