import React, { Component } from 'react';
import Nav from './components/Nav';
import Clock from 'react-clock';
import CalendarDay from './components/CalendarDay';
import WeatherMenu from './components/WeatherMenu';
import Input from './components/Input';
import ToDoList from './components/ToDoList';
import AdviceButton from './components/AdviceButton';
import AddressBook from './components/AddressBook';
import RandomColor from './components/RandomColor';
import FavoriteDogs from './components/FavoriteDogs';
import Racing from './components/Racing';
import Footer from './components/Footer';

class App extends Component {
  render () {
    return (
      <div>

        <Nav />

        <section className="main">
          <h2 className="top-title">
            Here is your personal dashboard, full of <i>dynamic</i> and <i>independent</i> components.
          </h2>

          <div className="container">

            <div className="row">

              <div className="col-xs-12 col-sm-6 col-lg-4 component-div-outer">
                <div className="component-div-inner">
                  <h3>Time and day</h3>
                  <h3 id="time-day">
                    <Clock />
                  </h3>
                  <br />
                  <CalendarDay />
                </div>
              </div>

              <div className="col-xs-12 col-sm-6 col-lg-4 component-div-outer">
                <div className="component-div-inner">
                  <h3>How is the weather today?</h3>
                  <br />
                  <WeatherMenu />
                </div>
              </div>

              <div className="col-xs-12 col-sm-6 col-lg-4 component-div-outer">
                <div className="component-div-inner">
                  <h3>What is on your mind?</h3>
                  <br />
                  <Input />
                </div>
              </div>

              <div className="col-xs-12 col-sm-6 col-lg-4 component-div-outer">
                <div className="component-div-inner">
                  <h3>What do you have to do?</h3>
                  <br />
                  <ToDoList />
                </div>
              </div>

              <div className="col-xs-12 col-sm-6 col-lg-4 component-div-outer">
                <div className="component-div-inner">
                  <h3>Get some advice</h3>
                  <br />
                  <AdviceButton />
                </div>
              </div>

              <div className="col-xs-12 col-sm-6 col-lg-4 component-div-outer">
                <div className="component-div-inner">
                  <h3>Address book</h3>
                  <br />
                  <AddressBook />
                </div>
              </div>

              <div className="col-xs-12 col-sm-6 col-lg-4 component-div-outer">
                <div className="component-div-inner" id="color-component">
                  <h3>Random color picker</h3>
                  <br />
                  <RandomColor />
                </div>
              </div>

              <div className="col-xs-12 col-sm-6 col-lg-4 component-div-outer">
                <div className="component-div-inner">
                  <h3>Favorite dogs</h3>
                  <br />
                  <FavoriteDogs />
                </div>
              </div>

              <div className="col-xs-12 col-sm-6 col-lg-4 component-div-outer">
                <div className="component-div-inner">
                  <h3>Car race</h3>
                  <br />
                  <Racing />
                </div>
              </div>

            </div>


          </div>

        </section>

        <Footer />

      </div>
    );
  }
}

export default App;